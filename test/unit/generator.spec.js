/**
 * (c) 2019 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { WebCryptoGenerator } from "../../";

describe( "WebCryptoGenerator", () => {
	it( "is available", () => {
		( WebCryptoGenerator != null ).should.be.true();
	} );

	describe( "exposes function for analysing password which", () => {
		it( "is a function", () => {
			WebCryptoGenerator.analysePassword.should.be.a.Function();
		} );

		it( "requires provision of password", () => {
			WebCryptoGenerator.analysePassword.should.have.length( 1 );

			( () => WebCryptoGenerator.analysePassword() ).should.throw();
		} );

		it( "requires provided password to be a string", () => {
			( () => WebCryptoGenerator.analysePassword( null ) ).should.throw();
			( () => WebCryptoGenerator.analysePassword( 1 ) ).should.throw();
			( () => WebCryptoGenerator.analysePassword( true ) ).should.throw();
			( () => WebCryptoGenerator.analysePassword( false ) ).should.throw();
			( () => WebCryptoGenerator.analysePassword( {} ) ).should.throw();
			( () => WebCryptoGenerator.analysePassword( [] ) ).should.throw();
			( () => WebCryptoGenerator.analysePassword( ["test"] ) ).should.throw();

			( () => WebCryptoGenerator.analysePassword( "test" ) ).should.not.throw();
		} );

		it( "returns object with report on provided password's complexity", () => {
			WebCryptoGenerator.analysePassword( "test" )
				.should.be.Object().which.has.properties( "password", "length", "recurring", "groups" );
		} );

		it( "returns provided password in property `password`", () => {
			WebCryptoGenerator.analysePassword( "test" ).password
				.should.be.String().which.is.equal( "test" );
		} );

		it( "returns number of characters in property `length`", () => {
			WebCryptoGenerator.analysePassword( "test" ).length
				.should.be.Number().which.is.equal( 4 );
		} );
	} );
} );
