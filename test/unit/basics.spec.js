/**
 * (c) 2019 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { WebCryptoGenerator, WebCryptoManager, WebCryptoIdentity } from "../../";
import { WebCryptoStore } from "../../src/store";

describe( "WebCrypto", function() {
	this.timeout( 10000 );

	let phrase;
	let portable;
	let imported;
	let loaded;

	before( () => WebCryptoStore.store.then( store => store.drop() ) );

	after( () => {
		return WebCryptoStore.store.then( store => {
			return store.drop().then( () => store.database.close() );
		} );
	} );

	it( "derives symmetric key from password", () => {
		return WebCryptoGenerator.passwordToSymmetricKey( "mySecret" )
			.then( key => {
				key.should.be.instanceOf( CryptoKey );

				phrase = key;
			} );
	} );

	it( "creates portable identity", () => {
		return WebCryptoGenerator.generatePortableIdentity( phrase )
			.then( created => {
				created.should.be.Object().which.has.size( 2 ).and.has.properties( "public", "private" );

				portable = created;
			} );
	} );

	it( "imports portable identity", () => {
		return WebCryptoIdentity.import( portable, phrase )
			.then( result => {
				imported = result;
			} );
	} );

	it( "saves portable identity in local store", () => {
		return imported.save( "my-identity" );
	} );

	it( "lists saved identity", () => {
		return WebCryptoManager.identities()
			.then( identities => {
				identities.should.be.Array().which.has.length( 1 );
				identities[0].should.be.instanceOf( WebCryptoIdentity );
				identities[0].name.should.be.equal( "my-identity" );

				loaded = identities[0];
			} );
	} );

	it( "rejects to save loaded identity again", () => {
		return loaded.save( "another-name" ).should.be.Promise().which.is.rejected();
	} );

	it( "accepts to drop loaded identity again", () => {
		return loaded.drop().should.be.Promise().which.is.resolved();
	} );

	it( "does not list saved identity after dropping it", () => {
		return WebCryptoManager.identities()
			.then( identities => {
				identities.should.be.Array().which.has.length( 0 );
			} );
	} );
} );
