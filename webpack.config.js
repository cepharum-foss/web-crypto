const path = require( "path" );

module.exports = {
	// mode: "production",
	mode: "development",
	entry: "./src/index.js",
	output: {
		path: path.resolve( __dirname, "dist" ),
		filename: "web-crypto.umd.js",
		library: "WebCrypto",
		libraryTarget: "umd",
	},
	target: "web",
};
