const Path = require( "path" );

module.exports = karma => karma.set( {
	frameworks: [
		"mocha",
		"should",
	],

	files: [
		"test/unit/**/*.spec.js",
	],

	reporters: [ "spec", "coverage-istanbul" ],

	concurrency: 1,

	browsers: ["ChromeHeadless"],

	customLaunchers: {
		ChromeHeadlessNoSandbox: {
			base: "ChromeHeadless",
			flags: ["--no-sandbox"],
		},
	},

	preprocessors: {
		"test/**/*.spec.js": ["webpack"],
	},

	singleRun: true,

	webpack: {
		mode: "development",
		module: {
			rules: [{
				test: /\.js$/,
				include: /src/,
				use: "coverage-istanbul-loader"
			}]
		},
		devtool: "inline-source-map",
	},

	coverageIstanbulReporter: {
		reports: [ "html", "text" ],
		dir: Path.join( __dirname, "coverage" ),
	},
} );
