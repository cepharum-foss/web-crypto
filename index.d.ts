export = WebCryptoModule;

declare module WebCryptoModule {
    class WebCryptoManager {
        static identities(): Promise<Array<WebCryptoIdentity>>;
    }

    class WebCryptoGenerator {
        static analysePassword( password: string, groupTests?: { [name: string]: RegExp } ): AnalysePasswordResult;
        static generatePortableIdentity( passphrase: CryptoKey, options?: GenerateIdentityOptions ): WebCryptoPortableIdentity;
    }

    class WebCryptoIdentity {
        constructor( usable: Array<CryptoKey>, portable: WebCryptoPortableIdentity, name?: string );

        static import( portable: WebCryptoPortableIdentity, passphrase: CryptoKey ): Promise<WebCryptoIdentity>;

        save( name: string ): Promise<WebCryptoIdentity>;
        drop(): Promise<WebCryptoIdentity>;

        name?: string;

        transmitToAll: CryptoKey;
        receiveFromAll: CryptoKey;
        signToAll: CryptoKey;
        verifyFromAll: CryptoKey;

        transmitFromAll: CryptoKey;
        receiveToAll: CryptoKey;
        signFromAll: CryptoKey;
        verifyToAll: CryptoKey;

        portable: WebCryptoPortableIdentity;
    }

    interface WebCryptoPortableIdentity {
        public: WebCryptoPortableIdentityPublic;
        private: WebCryptoPortableIdentityPrivate;
    }

    interface WebCryptoPortableIdentityPublic {
        receiveToAll: JsonWebKey;
        transmitFromAll: JsonWebKey;
        signFromAll: JsonWebKey;
        verifyToAll: JsonWebKey;
    }

    interface WebCryptoPortableIdentityPrivate {
        receiveFromAll: EncryptedJsonWebKey;
        transmitToAll: EncryptedJsonWebKey;
        signToAll: EncryptedJsonWebKey;
        verifyFromAll: EncryptedJsonWebKey;
    }

    interface WebCryptoIdentityPublic {
        receiveToAll: CryptoKey;
        transmitFromAll: CryptoKey;
        signFromAll: CryptoKey;
        verifyToAll: CryptoKey;
    }

    interface WebCryptoIdentityPrivate {
        receiveFromAll: CryptoKey;
        transmitToAll: CryptoKey;
        signToAll: CryptoKey;
        verifyFromAll: CryptoKey;
    }

    interface WebCryptoIdentityStoredRecord {
        name: string;
        usable: Array<CryptoKey>;
        portable: WebCryptoPortableIdentity;
    }

    interface AnalysePasswordResult {
        password: string;
        length: number;
        recurring: {
            max: number;
            sum: number;
            count: number;
            avg: number;
        };
        groups: { [name: string]: number; };
    }

    interface GenerateIdentityOptions {
        /**
         * Declares size of asymmetric keys in bits. Default is 4096.
         */
        size?: number;
    }

    /**
     * Describes encrypted JWK.
     */
    interface EncryptedJsonWebKey {
        jwk: JsonWebKey;
        iv: any;
    }
}
