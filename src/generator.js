/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */



const DefaultCharacterPool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.,_:;#+'*?=)(/&%$!^°<>[]{}";

const DefaultGroupTests = {
	lower: /[a-z]/,
	upper: /[A-Z]/,
	digit: /[0-9]/,
	special: /[-.,_:;<>#+'*?=)(/&%$"!{}[\]\\]/,
};

const TRANSMIT_TO_ALL = 0;
const RECEIVE_FROM_ALL = 1;
const SIGN_TO_ALL = 2;
const VERIFY_FROM_ALL = 3;

/**
 * Implements code for creating pass phrases for symmetric encryption as well as
 * identities.
 */
export class WebCryptoGenerator {
	/**
	 * Qualifies salt by either passing provided salt satisfying minimum
	 * requirements or generating salt using random sequence of characters.
	 *
	 * @param {?string} salt some existing key's salt to re-use (e.g. for verification of password, don't use same salt for different keys)
	 * @param {int} size minimum number of characters required in resulting salt
	 * @param {string} pool provides up to 65k characters available for use in resulting sequence
	 * @returns {string} provided salt satisfying requirements, or freshly generated random salt
	 */
	static qualifySalt( salt = null, { size = 16, pool = null } = {} ) {
		const _size = Math.max( 16, size );

		if ( typeof salt === "string" && !/\s/.test( salt ) && salt.length >= _size ) {
			return salt;
		}

		return this.generateRandomString( _size, { pool, buffer: true } );
	}

	/**
	 * Generates random string.
	 *
	 * @param {int} size number of characters in string to generate
	 * @param {string} pool set of characters to pick from randomly
	 * @param {boolean} buffer set true for returning result as buffer of octets
	 * @returns {string} resulting string
	 */
	static generateRandomString( size, { pool = null, buffer = false } = {} ) {
		const randomIndices = crypto.getRandomValues( new Uint16Array( size ) );
		const _pool = typeof pool === "string" && pool.length > 32 ? pool : DefaultCharacterPool;
		let result = buffer ? new Uint8Array( size ) : "";

		for ( let i = 0; i < size; i++ ) {
			if ( buffer ) {
				result[i] = _pool.charCodeAt( randomIndices[i] % _pool.length );
			} else {
				result += _pool.charAt( randomIndices[i] % _pool.length );
			}
		}

		return result;
	}

	/**
	 * Generates random IV for use with symmetric encryption.
	 *
	 * The resulting IV may be stored together with encrypted message. The same
	 * IV is required on decrypting the message.
	 *
	 * @param {int} size size of resulting IV in number of bits
	 * @returns {Uint8Array} sequence of random octets
	 */
	static generateIV( size = 96 ) {
		return crypto.getRandomValues( new Uint8Array( Math.ceil( size / 8 ) ) );
	}

	/**
	 * Generates AES key for authenticated symmetric encryption.
	 *
	 * The resulting key is provided in usable form as well as exported to raw
	 * format. The latter one is provided e.g. for signing and encrypting the
	 * key using asymmetric cryptography.
	 *
	 * @returns {PromiseLike<{raw, key}>} promises generated key in its usable form as well as its raw binary data
	 */
	static generateSymmetricKey() {
		return crypto.subtle.generateKey( {
			name: "AES-GCM",
			length: 256,
		}, true, [ "encrypt", "decrypt" ] )
			.then( exportable => crypto.subtle.exportKey( "raw", exportable ) )
			.then( exported => crypto.subtle.importKey( "raw", exported, { name: "AES-GCM" }, false, [ "encrypt", "decrypt" ] )
				.then( usable => {
					return {
						key: usable,
						raw: exported,
					};
				} ) );
	}

	/**
	 * Generates symmetric key for encrypting/decrypting from provided password.
	 *
	 * Providing same password and salt results in same key generated.
	 *
	 * `salt` might be omitted for generating random salt. However, you should
	 * create, store and provide random salt if you intend to use resulting key
	 * for verifying a password by comparing the derived key.
	 *
	 * @param {string} password user-provided password
	 * @param {string} salt public salt for generating different keys from identical passwords used
	 * @param {int} iterations number of iterations to apply for deriving key from password (minimum is 100000)
	 * @param {true} forKeyWrapping true if resulting key will be used for wrapping/unwrapping keys instead of encrypting/decrypting messages
	 * @returns {PromiseLike<CryptoKey>} promises resulting key suitable for symmetric encryption/decryption
	 */
	static passwordToSymmetricKey( password, { salt = null, iterations = 10000, forKeyWrapping = true } = {} ) {
		if ( !window.isSecureContext ) {
			return Promise.reject( new TypeError( "code disabled due to insecure context" ) );
		}

		const encoder = new TextEncoder();

		return crypto.subtle.importKey( "raw", encoder.encode( password ), "PBKDF2", false, [ "deriveBits", "deriveKey" ] )
			.then( token => {
				const _salt = this.qualifySalt( salt );
				const _iterations = Math.max( 1000000, Math.min( 100, parseInt( iterations ) || 0 ) );

				return crypto.subtle.deriveKey( {
					name: "PBKDF2",
					salt: _salt,
					iterations: _iterations,
					hash: "SHA-384",
				}, token, {
					name: "AES-GCM",
					length: 256,
				}, false, forKeyWrapping ? [ "wrapKey", "unwrapKey" ] : [ "encrypt", "decrypt" ] );
			} );
	}

	/**
	 * @typedef {object} PasswordAnalysis
	 * @property {string} password normalized password, always use this one instead of provided one
	 * @property {int} length number of characters in password
	 * @property {object<string,int>} groups character count per group of characters
	 * @property {{avg: number, max: number, count: number, sum: number}} recurring statistical information on recurring characters in password
	 */

	/**
	 * Calculates information on provided password suitable for assessing its
	 * strength.
	 *
	 * @param {string} password password to assess
	 * @param {object<string,RegExp>} groupTests custom tests for discovering groups of characters used in password
	 * @returns {PasswordAnalysis} analysis results
	 */
	static analysePassword( password, groupTests = null ) {
		if ( typeof password !== "string" ) {
			throw new TypeError( "provided password must be string" );
		}

		const _password = password.trim();
		const _tests = groupTests || DefaultGroupTests;
		const groupNames = Object.keys( _tests );
		const numGroups = groupNames.length;
		const numCharacters = _password.length;
		const characters = {};
		const groups = {};

		for ( let i = 0; i < numCharacters; i++ ) {
			const char = _password.charAt( i );

			if ( characters.hasOwnProperty( char ) ) {
				characters[char]++;
			} else {
				characters[char] = 1;
			}

			for ( let j = 0; j < numGroups; j++ ) {
				const name = groupNames[j];

				if ( _tests[name].test( char ) ) {
					groups[name] = ( groups[name] || 0 ) + 1;
				}
			}
		}

		const recurring = {
			max: 0,
			sum: 0,
			count: 0,
			avg: 0,
		};

		for ( const char of Object.keys( characters ) ) {
			const count = characters[char];

			recurring.max = Math.max( recurring.max, count );
			recurring.sum += count;
			recurring.count++;
		}

		recurring.avg = recurring.sum / recurring.count;

		return {
			password: _password,
			length: _password.length,
			recurring,
			groups,
		};
	}

	/**
	 * Generates portable identity.
	 *
	 * In context of this web-crypto library, an identity consists of multiple
	 * sets of keys for transmitting and receiving ciphers. The provided
	 * passphrase is required to encrypt some of the returned keys for writing
	 * to remote or external storage such as servers or local file systems.
	 *
	 * A _portable_ identity must be imported as instance of WebCryptoIdentity
	 * to become locally usable. @see WebCryptIdentity.import()
	 *
	 * @param {CryptoKey} passphrase key for encrypting private parts of returned identity, @see WebCryptoGenerator.passwordToSymmetricKey()
	 * @param {int} size size of keys in bits
	 * @returns {Promise<WebCryptoPortableIdentity>} promises resulting identity
	 */
	static generatePortableIdentity( passphrase, { size = 4096 } = {} ) {
		if ( !window.isSecureContext ) {
			return Promise.reject( new TypeError( "code disabled due to insecure context" ) );
		}

		if ( !( passphrase instanceof CryptoKey ) ) {
			return Promise.reject( new TypeError( "provided passphrase must be instance of CryptoKey" ) );
		}

		if ( passphrase.usages.indexOf( "wrapKey" ) < 0 ) {
			return Promise.reject( new TypeError( "provided passphrase is not suitable for wrapping keys" ) );
		}

		const encryptionMode = {
			name: "RSA-OAEP",
			modulusLength: size,
			publicExponent: new Uint8Array( [ 1, 0, 1 ] ),
			hash: "SHA-384",
		};

		const signatureMode = {
			name: "RSA-PSS",
			modulusLength: size,
			publicExponent: new Uint8Array( [ 1, 0, 1 ] ),
			hash: "SHA-384",
		};

		// step 1: create exportable pairs of keys
		return Promise.all( [
			crypto.subtle.generateKey(
				encryptionMode,
				true,
				[ "encrypt", "decrypt" ]
			),
			crypto.subtle.generateKey(
				encryptionMode,
				true,
				[ "encrypt", "decrypt" ]
			),
			crypto.subtle.generateKey(
				signatureMode,
				true,
				[ "sign", "verify" ]
			),
			crypto.subtle.generateKey(
				signatureMode,
				true,
				[ "sign", "verify" ]
			),
		] )
			.then( pairs => {
				// step 2: export/wrap keys of either pair
				const ivList = [
					this.generateIV( 96 ),
					this.generateIV( 96 ),
					this.generateIV( 96 ),
					this.generateIV( 96 ),
				];

				return Promise.all( [
					// wrap (encrypt) keys used by identity owner
					crypto.subtle.wrapKey( "jwk", pairs[TRANSMIT_TO_ALL].publicKey, passphrase, {
						name: "AES-GCM",
						iv: ivList[TRANSMIT_TO_ALL],
					} ),
					crypto.subtle.wrapKey( "jwk", pairs[RECEIVE_FROM_ALL].privateKey, passphrase, {
						name: "AES-GCM",
						iv: ivList[RECEIVE_FROM_ALL],
					} ),
					crypto.subtle.wrapKey( "jwk", pairs[SIGN_TO_ALL].privateKey, passphrase, {
						name: "AES-GCM",
						iv: ivList[SIGN_TO_ALL],
					} ),
					crypto.subtle.wrapKey( "jwk", pairs[VERIFY_FROM_ALL].publicKey, passphrase, {
						name: "AES-GCM",
						iv: ivList[VERIFY_FROM_ALL],
					} ),

					// export (w/o encryption) keys used by peers of identity owner
					crypto.subtle.exportKey( "jwk", pairs[TRANSMIT_TO_ALL].privateKey ),
					crypto.subtle.exportKey( "jwk", pairs[RECEIVE_FROM_ALL].publicKey ),
					crypto.subtle.exportKey( "jwk", pairs[SIGN_TO_ALL].publicKey ),
					crypto.subtle.exportKey( "jwk", pairs[VERIFY_FROM_ALL].privateKey ),
				] )
					.then( exported => {
						return {
							private: {
								transmitToAll: { jwk: exported[TRANSMIT_TO_ALL], iv: ivList[TRANSMIT_TO_ALL] },
								receiveFromAll: { jwk: exported[RECEIVE_FROM_ALL], iv: ivList[RECEIVE_FROM_ALL] },
								signToAll: { jwk: exported[SIGN_TO_ALL], iv: ivList[SIGN_TO_ALL] },
								verifyFromAll: { jwk: exported[VERIFY_FROM_ALL], iv: ivList[VERIFY_FROM_ALL] },
							},
							public: {
								receiveToAll: exported[4 + TRANSMIT_TO_ALL],
								transmitFromAll: exported[4 + RECEIVE_FROM_ALL],
								verifyToAll: exported[4 + SIGN_TO_ALL],
								signFromAll: exported[4 + VERIFY_FROM_ALL],
							},
						};
					} );
			} );
	}
}
