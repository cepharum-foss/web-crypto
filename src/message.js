/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { WebCryptoGenerator } from "./generator";
import { WebCryptoIdentity } from "./identity";

/**
 * Implements functionality regarding transmission and reception of encrypted
 * and signed messages.
 */
export class WebCryptoMessage {
	/**
	 * Encrypts message to be send to the public or to one or more
	 *
	 * @note This function is applying E/S/E as proposed in section 5.3 of
	 *       http://world.std.com/~dtd/sign_encrypt/sign_encrypt7.html for it
	 *       relies on capability to hash the message which is known here instead
	 *       of the recipient's key which is sort of opaque to this library.
	 *
	 * @param {string} message message to be encrypted for transmission
	 * @param {WebCryptoIdentity} sender identity of message sender
	 * @param {WebCryptoIdentity} recipient identity of message recipient, omit for broadcast message
	 * @returns {Promise<string>} encrypted message
	 */
	static transmit( message, sender, recipient = null ) {
		if ( !message || typeof message !== "string" ) {
			return Promise.reject( new TypeError( "message must be string" ) );
		}

		if ( !( sender instanceof WebCryptoIdentity ) ) {
			return Promise.reject( new TypeError( "missing sender identity" ) );
		}

		if ( !sender.isPrivate ) {
			return Promise.reject( new TypeError( "sender identity is missing private part" ) );
		}

		if ( recipient != null && !( recipient instanceof WebCryptoIdentity ) ) {
			return Promise.reject( new TypeError( "invalid recipient identity" ) );
		}

		// prepare parameters
		const encryptionKey = recipient == null ? sender.transmitToAll : recipient.transmitFromAll;
		const signatureKey = recipient == null ? sender.signToAll : recipient.signFromAll;
		const rawMessage = TextEncoder().encode( message );

		return Promise.all( [
			WebCryptoGenerator.generateSymmetricKey(),
			crypto.subtle.digest( "SHA-512", rawMessage ),
			WebCryptoGenerator.generateIV( 96 ),
		] )
			.then( ( [ symmetric, messageDigest, iv ] ) => {
				return crypto.subtle.encrypt( {
					name: "AES-GCM",
					iv,
				}, symmetric.key, rawMessage )
					.then( encryptedMessage => {
						return crypto.subtle.sign( )
					} );
			} );

		// encrypt message
		const innerkey = generatekey();
		const encrypted = encrypt(message, innerkey);

		// sign encrypted message
		const signed = encrypted;

		// append hash of plaintext message
		const qualified = hash(message) + signed;

		// encrypt qualified message once again
		const outerkey = generatekey();
		const reencrypted = encrypt(qualified, outerkey);

		return reencrypted;
	}
}
