/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { WebCryptoStore } from "./store";

// private part:
const TRANSMIT_TO_ALL = 0;
const RECEIVE_FROM_ALL = 1;
const SIGN_FOR_ALL = 2;
const VERIFY_FROM_ALL = 3;

// public part:
const TRANSMIT_FROM_ALL = 4;
const RECEIVE_FOR_ALL = 5;
const SIGN_FROM_ALL = 6;
const VERIFY_FOR_ALL = 7;


/**
 * Represents single identity at runtime.
 */
export class WebCryptoIdentity {
	/**
	 * @param {CryptoKey[]} usable set of previously imported keys, ready for use
	 * @param {WebCryptoPortableIdentity} portable exported/wrapped keys
	 * @param {string} name unique name of identity used in local store, omit unless read from store
	 */
	constructor( usable, portable, name = null ) {
		let _name = name ? String( name ).trim() : null;

		if ( !Array.isArray( usable ) || ( usable.length !== 4 && usable.length !== 8 ) ) {
			throw new TypeError( "invalid number of usable keys" );
		}

		for ( let i = 0; i < usable.length; i++ ) {
			if ( !( usable[i] instanceof CryptoKey ) ) {
				throw new TypeError( "invalid instance of CryptoKey" );
			}
		}

		Object.defineProperties( this, {
			/**
			 * Exposes name of identity used in local store.
			 *
			 * @name WebCryptoIdentity#name
			 * @prop {?string}
			 */
			name: {
				get: () => _name,
				set: newName => {
					if ( _name != null ) {
						throw new Error( "must not change identity's name in local store" );
					}

					if ( !newName || typeof newName !== "string" || newName.trim() === "" ) {
						throw new TypeError( "invalid name for use with storing identity in local store" );
					}

					_name = newName;
				},
				enumerable: true,
			},

			/**
			 * Exposes crypto key for decrypting broadcast messages sent by
			 * current identity.
			 *
			 * @name WebCryptoIdentity#receiveToAll
			 * @prop CryptoKey
			 * @readonly
			 */
			receiveToAll: {
				value: usable[RECEIVE_FOR_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for encrypting private messages to current
			 * identity.
			 *
			 * @name WebCryptoIdentity#transmitFromAll
			 * @prop CryptoKey
			 * @readonly
			 */
			transmitFromAll: {
				value: usable[TRANSMIT_FROM_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for verifying integrity of broadcast messages
			 * sent by current identity.
			 *
			 * @name WebCryptoIdentity#verifyToAll
			 * @prop CryptoKey
			 * @readonly
			 */
			verifyToAll: {
				value: usable[VERIFY_FOR_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for signing private messages sent to current
			 * identity for proofing their integrity on reception.
			 *
			 * @note This key has very limited benefits for securing replies to
			 *       broadcast messages against modification for this key is
			 *       available to the public.
			 *
			 * @name WebCryptoIdentity#receiveToAll
			 * @prop CryptoKey
			 * @readonly
			 */
			signFromAll: {
				value: usable[SIGN_FROM_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for decrypting replies to broadcast messages
			 * sent by current identity before.
			 *
			 * @name WebCryptoIdentity#receiveFromAll
			 * @prop CryptoKey
			 * @readonly
			 */
			receiveFromAll: {
				value: usable[RECEIVE_FROM_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for encrypting broadcast messages sent by
			 * current identity.
			 *
			 * @name WebCryptoIdentity#transmitToAll
			 * @prop CryptoKey
			 * @readonly
			 */
			transmitToAll: {
				value: usable[TRANSMIT_TO_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for verifying integrity of private messages
			 * sent to current identity.
			 *
			 * @name WebCryptoIdentity#verifyFromAll
			 * @prop CryptoKey
			 * @readonly
			 */
			verifyFromAll: {
				value: usable[VERIFY_FROM_ALL],
				enumerable: true,
			},

			/**
			 * Exposes crypto key for signing broadcast messages sent by current
			 * identity for verifying its integrity on reception.
			 *
			 * @name WebCryptoIdentity#verifyFromAll
			 * @prop CryptoKey
			 * @readonly
			 */
			signToAll: {
				value: usable[SIGN_FOR_ALL],
				enumerable: true,
			},

			/**
			 * Exposes full identity in its portable format.
			 *
			 * A portable identity is containing public and optionally private
			 * keys with the latter ones protected by symmetric encryption, thus
			 * enabling its processing in less secure contexts.
			 *
			 * @name WebCryptoIdentity#portable
			 * @property {WebCryptoPortableIdentity}
			 * @readonly
			 */
			portableIdentity: {
				value: Object.freeze( Object.assign( {}, {
					public: Object.freeze( Object.assign( {}, portable.public ) ),
					private: portable.private ? Object.freeze( Object.assign( {}, portable.private ) ) : undefined,
				} ) ),
				enumerable: true,
			},

			/**
			 * Exposes public part of identity in its portable format.
			 *
			 * A portable identity is containing public and optionally private
			 * keys with the latter ones protected by symmetric encryption, thus
			 * enabling its processing in less secure contexts.
			 *
			 * @name WebCryptoIdentity#portable
			 * @property {WebCryptoPortableIdentityPublic}
			 * @readonly
			 */
			shareableIdentity: {
				value: Object.freeze( Object.assign( {}, portable.public ) ),
				enumerable: true,
			},

			/**
			 * Indicates if current identity includes private keys.
			 *
			 * @name WebCryptoIdentity#isPrivate
			 * @property boolean
			 * @readonly
			 */
			isPrivate: {
				value: usable.length === 8,
				enumerable: true,
			},
		} );
	}

	/**
	 * Tests provided set of CryptoKey instances for matching expected format.
	 *
	 * @param {object} record set of keys to test
	 * @returns {void}
	 * @throws TypeError if provided record does not comply with expected format
	 */
	static validateRecord( record ) {
		if ( !record ) {
			throw new TypeError( "missing identity record" );
		}

		if ( !record.public ) {
			throw new TypeError( "missing public part of identity record" );
		}

		const groups = Object.keys( record );

		if ( !record.private && groups.length > 1 ) {
			throw new TypeError( "invalid extra information in identity record" );
		}

		if ( record.private && groups.length > 2 ) {
			throw new TypeError( "invalid extra information in identity record" );
		}

		if ( Object.keys( record.public ).length !== 4 ) {
			throw new TypeError( "invalid number of keys in public part of identity record" );
		}

		if ( !( record.public.receiveToAll instanceof CryptoKey ) ||
		     !( record.public.transmitFromAll instanceof CryptoKey ) ||
		     !( record.public.verifyToAll instanceof CryptoKey ) ||
		     !( record.public.signFromAll instanceof CryptoKey )
		) {
			throw new TypeError( "invalid type of keys in public part of identity record" );
		}

		if ( record.public.receiveToAll.extractable ||
		     record.public.transmitFromAll.extractable ||
		     record.public.verifyToAll.extractable ||
		     record.public.signFromAll.extractable ) {
			throw new TypeError( "public keys must not be extractable" );
		}

		if ( record.private ) {
			if ( Object.keys( record.private ).length !== 4 ) {
				throw new TypeError( "invalid number of keys in private part of identity record" );
			}

			if ( !( record.private.receiveFromAll instanceof CryptoKey ) ||
			     !( record.private.transmitToAll instanceof CryptoKey ) ||
			     !( record.private.verifyFromAll instanceof CryptoKey ) ||
			     !( record.private.signToAll instanceof CryptoKey )
			) {
				throw new TypeError( "invalid type of keys in private part of identity record" );
			}

			if ( record.private.receiveFromAll.extractable ||
			     record.private.transmitToAll.extractable ||
			     record.private.verifyFromAll.extractable ||
			     record.private.signToAll.extractable ) {
				throw new TypeError( "private keys must not be extractable" );
			}
		}
	}

	/**
	 * Imports public part of provided identity.
	 *
	 * @param {WebCryptoPortableIdentityPublic} portable public part of portable identity to import
	 * @returns {Promise<WebCryptoIdentityPublic>} locally usable keys of provided identity
	 * @protected
	 */
	static importPublic( portable ) {
		if ( !window.isSecureContext ) {
			return Promise.reject( new TypeError( "code disabled due to insecure context" ) );
		}

		if ( !portable || !portable.transmitFromAll || !portable.receiveToAll ||
		     !portable.signFromAll || !portable.verifyToAll ) {
			return Promise.reject( new TypeError( "invalid identity" ) );
		}

		return Promise.all( [
			crypto.subtle.importKey( "jwk", portable.transmitFromAll, {
				name: "RSA-OAEP",
				hash: "SHA-384",
			}, false, ["encrypt"] ),
			crypto.subtle.importKey( "jwk", portable.receiveToAll, {
				name: "RSA-OAEP",
				hash: "SHA-384",
			}, false, ["decrypt"] ),
			crypto.subtle.importKey( "jwk", portable.signFromAll, {
				name: "RSA-PSS",
				hash: "SHA-384",
			}, false, ["sign"] ),
			crypto.subtle.importKey( "jwk", portable.verifyToAll, {
				name: "RSA-PSS",
				hash: "SHA-384",
			}, false, ["verify"] ),
		] )
			.then( ( [ transmitFromAll, receiveToAll, signFromAll, verifyToAll ] ) => {
				return {
					transmitFromAll,
					receiveToAll,
					signFromAll,
					verifyToAll,
				};
			} );
	}

	/**
	 * Imports private part of provided identity.
	 *
	 * @param {WebCryptoPortableIdentityPrivate} portable private part of portable identity to import
	 * @param {CryptoKey} passphrase symmetric key for decrypting private part of provided identity, @see WebCryptoGenerator.passwordToSymmetricKey()
	 * @returns {Promise<WebCryptoIdentityPrivate>} locally usable keys of provided identity
	 * @protected
	 */
	static unwrapPrivate( portable, passphrase ) {
		if ( !window.isSecureContext ) {
			return Promise.reject( new TypeError( "code disabled due to insecure context" ) );
		}

		if ( !portable || !portable.transmitToAll || !portable.receiveFromAll ||
		     !portable.signToAll || !portable.verifyFromAll ) {
			return Promise.reject( new TypeError( "invalid identity" ) );
		}

		if ( !( passphrase instanceof CryptoKey ) ) {
			return Promise.reject( new TypeError( "provided passphrase must be instance of CryptoKey" ) );
		}

		if ( passphrase.usages.indexOf( "unwrapKey" ) < 0 ) {
			return Promise.reject( new TypeError( "provided passphrase is not suitable for unwrapping keys" ) );
		}

		return Promise.all( [
			crypto.subtle.unwrapKey( "jwk", portable.transmitToAll.jwk, passphrase, {
				name: "AES-GCM",
				iv: portable.transmitToAll.iv,
			}, {
				name: "RSA-OAEP",
				hash: "SHA-384",
			}, false, ["encrypt"] ),
			crypto.subtle.unwrapKey( "jwk", portable.receiveFromAll.jwk, passphrase, {
				name: "AES-GCM",
				iv: portable.receiveFromAll.iv,
			}, {
				name: "RSA-OAEP",
				hash: "SHA-384",
			}, false, ["decrypt"] ),
			crypto.subtle.unwrapKey( "jwk", portable.signToAll.jwk, passphrase, {
				name: "AES-GCM",
				iv: portable.signToAll.iv,
			}, {
				name: "RSA-PSS",
				hash: "SHA-384",
			}, false, ["sign"] ),
			crypto.subtle.unwrapKey( "jwk", portable.verifyFromAll.jwk, passphrase, {
				name: "AES-GCM",
				iv: portable.verifyFromAll.iv,
			}, {
				name: "RSA-PSS",
				hash: "SHA-384",
			}, false, ["verify"] ),
		] )
			.then( ( [ transmitToAll, receiveFromAll, signToAll, verifyFromAll ] ) => {
				return {
					transmitToAll,
					receiveFromAll,
					signToAll,
					verifyFromAll,
				};
			} );
	}

	/**
	 * Imports identity for local use.
	 *
	 * @param {WebCryptoPortableIdentity} portable portable identity
	 * @param {CryptoKey} passphrase key for decrypting private parts of identity, @see WebCryptoGenerator.passwordToSymmetricKey()
	 * @returns {Promise<WebCryptoIdentity>} promises imported identity ready for use
	 */
	static import( portable, passphrase = null ) {
		if ( !window.isSecureContext ) {
			return Promise.reject( new Error( "code disabled due to insecure context" ) );
		}

		if ( !portable || !portable.public ) {
			return Promise.reject( new TypeError( "invalid identity" ) );
		}

		return Promise.all( [
			portable.private && passphrase ? this.unwrapPrivate( portable.private, passphrase ) : null,
			this.importPublic( portable.public ),
		] )
			.then( ( [ unwrapped, imported ] ) => new WebCryptoIdentity( [
				unwrapped.transmitToAll,
				unwrapped.receiveFromAll,
				unwrapped.signToAll,
				unwrapped.verifyFromAll,

				imported.transmitFromAll,
				imported.receiveToAll,
				imported.signFromAll,
				imported.verifyToAll,
			], Object.freeze( {
				public: Object.freeze( portable.public ),
				private: Object.freeze( portable.private ),
			} ) ) );
	}

	/**
	 * Recovers named identity from stored record.
	 *
	 * @param {WebCryptoIdentityStoredRecord} record stored record of identity
	 * @returns {WebCryptoIdentity} recovered identity
	 */
	static recover( record ) {
		if ( !window.isSecureContext ) {
			throw new Error( "code disabled due to insecure context" );
		}

		if ( !record || !Array.isArray( record.usable ) || !record.portable || !record.portable.public ) {
			throw new TypeError( "invalid record to recover identity from" );
		}

		if ( !record.name || typeof record.name !== "string" || record.name.trim() === "" ) {
			throw new TypeError( "invalid name of identity to be recovered" );
		}

		return new WebCryptoIdentity( record.usable, record.portable, record.name );
	}

	/**
	 * Writes current identity into store.
	 *
	 * @param {string} name unique name of identity to use in store
	 * @returns {Promise<WebCryptoIdentity>} promises current identity saved in store
	 */
	save( name ) {
		if ( this.name != null ) {
			return Promise.reject( new Error( "identity has been read from local store before, already" ) );
		}

		if ( typeof name !== "string" || !name.trim().length ) {
			return Promise.reject( new TypeError( "invalid name of identity" ) );
		}

		return WebCryptoStore.store
			.then( store => store.write( name, {
				name, usable: [
					this.transmitToAll,
					this.receiveFromAll,
					this.signToAll,
					this.verifyFromAll,
					this.transmitFromAll,
					this.receiveToAll,
					this.signFromAll,
					this.verifyToAll,
				], portable: this.portable
			} ) )
			.then( () => this );
	}

	/**
	 * Removes current identity from local store.
	 *
	 * @returns {Promise<WebCryptoIdentity>} promises current identity removed from store
	 */
	drop() {
		if ( this.name == null ) {
			return Promise.reject( new Error( "identity has not been read from local store before" ) );
		}

		return WebCryptoStore.store
			.then( store => store.remove( this.name ) )
			.then( () => this );
	}
}
