/**
 * (c) 2019 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import { WebCryptoIdentity } from "./identity";
import { WebCryptoStore } from "./store";

/**
 * Exposes basic API for managing locally available identities for encrypted
 * communication.
 */
export class WebCryptoManager {
	/**
	 * Lists identities locally available in current domain.
	 *
	 * @returns {Promise<Array<WebCryptoIdentity>>} promises identities found in store
	 */
	static identities() {
		return WebCryptoStore.store
			.then( store => store.list() )
			.then( ( { items } ) => {
				const numItems = items.length;
				const filtered = new Array( numItems );
				let write = 0;

				for ( let read = 0; read < numItems; read++ ) {
					const { key, record } = items[read];

					try {
						filtered[write++] = WebCryptoIdentity.recover( record, key );
					} catch ( error ) {
						console.warn( `recovering identity ${key} failed: ${error.message}` );
					}
				}

				filtered.splice( write );

				return filtered;
			} );
	}
}
