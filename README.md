# deprecated ... use [@cepharum/web-crypto-revised](https://gitlab.com/cepharum-foss/web-crypto-revised) instead

# web-crypto

convenience wrapper for [browsers' WebCrypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API)

## License

MIT
